var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser")

var app = express()  // make express app
var server = require('http').createServer(app) // inject app into the server

app.get('/', function (req, res) {
    res.send('This is starting app for Guestbook')
})
// set up the view engine
// manage our entries
// set up the logger
// GETS
// POSTS
// 404

// Listen for an application request on port 8081
server.listen(8889, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/')
})